﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DeliveryMan.Tests
{
    [TestClass]
    public class DbHelperTests
    {
        [TestMethod]
        public void GetAllItems_ReturnsItemsList()
        {
            DbHelper helper = new DbHelper();

            var items = helper.GetAllItems();

            Assert.IsNotNull(items);
        }

        [TestMethod]
        public void GetAllVendors_ReturnsVendorsList()
        {
            DbHelper helper = new DbHelper();

            var vendors = helper.GetAllVendors();

            Assert.IsNotNull(vendors);
        }
    }
}
