$(function() {

    Morris.Area({
        element: 'morris-area-chart',
        data: [{
            period: '2013 Q1',
            iphone: 2666,
            ipad: null,
            itouch: 2647,
            brbad: 5000
        }, {
            period: '2013 Q2',
            iphone: 2778,
            ipad: 2294,
            itouch: 2441,
            brbad: 9000

        }, {
            period: '2013 Q3',
            iphone: 4912,
            ipad: 1969,
            itouch: 2501,
            brbad: 800
        }, {
            period: '2013 Q4',
            iphone: 3767,
            ipad: 3597,
            itouch: 5689,
            brbad: 600
        }, {
            period: '2014 Q1',
            iphone: 6810,
            ipad: 1914,
            itouch: 2293,
            brbad: 1100
        }, {
            period: '2014 Q2',
            iphone: 5670,
            ipad: 4293,
            itouch: 1881,
            brbad: 1000
        }, {
            period: '2014 Q3',
            iphone: 4820,
            ipad: 3795,
            itouch: 1588,
            brbad: 1150
        }, {
            period: '2014 Q4',
            iphone: 15073,
            ipad: 5967,
            itouch: 5175,
            brbad: 1023

        }],
        xkey: 'period',
        ykeys: ['iphone', 'ipad', 'itouch', 'brbad'],
        labels: ['iPhone', 'iPad', 'iPod', 'T-Shirt Breaking Bad'],
        pointSize: 2,
        hideHover: 'auto',
        resize: true
    });

    Morris.Donut({
        element: 'morris-donut-chart',
        data: [{
            label: "Asia",
            value: 40
        }, {
            label: "Europe",
            value: 60
        }, {
            label: "America",
            value: 30
        },
        {
            label: "Africa",
            value: 30
        }
        ],
        resize: true
    });

    Morris.Bar({
        element: 'morris-bar-chart',
        data: [{
            y: '2006',
            a: 100,
            b: 90
        }, {
            y: '2007',
            a: 75,
            b: 65
        }, {
            y: '2008',
            a: 50,
            b: 40
        }, {
            y: '2009',
            a: 75,
            b: 65
        }, {
            y: '2013',
            a: 50,
            b: 40
        }, {
            y: '2014',
            a: 75,
            b: 65
        }, {
            y: '2012',
            a: 100,
            b: 90
        }],
        xkey: 'y',
        ykeys: ['a', 'b'],
        labels: ['Series A', 'Series B'],
        hideHover: 'auto',
        resize: true
    });

});
