﻿using DeliveryMan.Models;
using Microsoft.AnalysisServices.AdomdClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DeliveryMan
{
    public class DbHelper
    {
        AdomdConnection connection = new AdomdConnection(@"Data Source=NOTEBOOK-PC\SQLENTERPRISE;Initial Catalog=MultiTest;");
        AdomdCommand command;
        AdomdDataReader reader;

        public IList<Item> GetAllItems()
        {
            List<Item> items = new List<Item>();

            command = new AdomdCommand("SELECT { [Measures].[Count] } ON COLUMNS, { ([Item].[Name].[Name].ALLMEMBERS * [Item].[Price].[Price].ALLMEMBERS * [Item].[Category Name].[Category Name].ALLMEMBERS * [Item].[Company Name].[Company Name].ALLMEMBERS * [Item].[Country].[Country].ALLMEMBERS ) }ON ROWS FROM ( SELECT ( -{ [Item].[Name].[All].UNKNOWNMEMBER } ) ON COLUMNS FROM [Delivery DB]) ", connection);

            connection.Open();

            reader = command.ExecuteReader();

            while (reader.Read())
            {
                items.Add(new Item(reader.GetString(0), reader.GetDouble(1), reader.GetString(3), reader.GetString(2), reader.GetString(4)));
            }

            connection.Close();

            return items;
        }

        public IList<Vendor> GetAllVendors()
        {
            List<Vendor> vendors = new List<Vendor>();

            command = new AdomdCommand("SELECT { [Measures].[Count] } ON COLUMNS, { ([Item].[Company Name].[Company Name].ALLMEMBERS * [Item].[City].[City].ALLMEMBERS * [Item].[Country].[Country].ALLMEMBERS ) }  ON ROWS FROM ( SELECT ( -{ [Item].[Company Name].[All].UNKNOWNMEMBER } ) ON COLUMNS FROM [Delivery DB]) ", connection);

            connection.Open();

            reader = command.ExecuteReader();

            while (reader.Read())
            {
                vendors.Add(new Vendor(reader.GetString(0), reader.GetString(1), reader.GetString(2)));
            }

            connection.Close();

            return vendors;
        }

        public IList<string> GetAllCategories()
        {
            List<string> categories = new List<string>();

            command = new AdomdCommand(" SELECT { [Measures].[Count] } ON COLUMNS, NON EMPTY { ([Item].[Category Name].[Category Name].ALLMEMBERS ) } ON ROWS FROM [Delivery DB]  ", connection);

            connection.Open();

            reader = command.ExecuteReader();

            while (reader.Read())
            {
                categories.Add(reader.GetString(0));
            }
            
            connection.Close();

            return categories;
        }

        public IList<Order> GetAllOrders()
        {
            List<Order> vendors = new List<Order>();

            command = new AdomdCommand("SELECT NON EMPTY { [Measures].[Count] } ON COLUMNS, NON EMPTY { ([Client].[Id].[Id].ALLMEMBERS * [Item].[Name].[Name].ALLMEMBERS * [Item].[Price].[Price].ALLMEMBERS * [Delivery Type].[Id].[Id].ALLMEMBERS * [Place Category].[Id].[Id].ALLMEMBERS ) } ON ROWS FROM [Delivery DB] ", connection);

            connection.Open();

            reader = command.ExecuteReader();

            while (reader.Read())
            {
                vendors.Add(new Order(reader.GetString(0), reader.GetString(1), reader.GetDouble(2), reader.GetString(3), reader.GetString(4), reader.GetInt32(5)));
            }

            connection.Close();

            return vendors;
        }

        public IList<string> GetAllClients()
        {
            List<string> clients = new List<string>();

            command = new AdomdCommand("SELECT { [Measures].[Count] } ON COLUMNS, { ([Client].[Id].[Id].ALLMEMBERS ) }  ON ROWS FROM [Delivery DB]", connection);

            connection.Open();

            reader = command.ExecuteReader();

            while (reader.Read())
            {
                clients.Add(reader.GetString(0));
            }

            connection.Close();

            return clients;
        }

        public IList<string> GetAllTypes()
        {
            List<string> types = new List<string>();

            command = new AdomdCommand("SELECT { [Measures].[Count] } ON COLUMNS, { ([Delivery Type].[Id].[Id].ALLMEMBERS ) } ON ROWS FROM ( SELECT ( -{ [Delivery Type].[Id].[All].UNKNOWNMEMBER } ) ON COLUMNS FROM [Delivery DB]) ", connection);

            connection.Open();

            reader = command.ExecuteReader();

            while (reader.Read())
            {
                types.Add(reader.GetString(0));
            }

            connection.Close();

            return types;
        }

    }
}