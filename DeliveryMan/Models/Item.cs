﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DeliveryMan.Models
{
    public class Item
    {
        private string _name;

        /// <summary>
        /// Название товара
        /// </summary>
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private double _price;

        /// <summary>
        /// Цена товара
        /// </summary>
        public double Price
        {
            get { return _price; }
            set { _price = value; }
        }

        private string _vendor;

        /// <summary>
        /// Название компании поставщика
        /// </summary>
        public string Vendor
        {
            get { return _vendor; }
            set { _vendor = value; }
        }

        private string _category;

        /// <summary>
        /// Категория товара
        /// </summary>
        public string Category
        {
            get { return _category; }
            set { _category = value; }
        }

        private string _country;

        /// <summary>
        /// Страна поставщика
        /// </summary>
        public string Country
        {
            get { return _country; }
            set { _country = value; }
        }
        

        public Item(string name, double price, string vendor, string category, string country)
        {
            Name = name;
            Price = price;
            Vendor = vendor;
            Category = category;
            Country = country;
        }
        
    }
}