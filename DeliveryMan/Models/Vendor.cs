﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DeliveryMan.Models
{
    public class Vendor
    {
        private string _name;

        /// <summary>
        /// Название компании поставщика
        /// </summary>
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private string _country;

        /// <summary>
        /// Страна поставщика
        /// </summary>
        public string Country
        {
            get { return _country; }
            set { _country = value; }
        }

        private string _city;

        /// <summary>
        /// Город поставщика
        /// </summary>
        public string City
        {
            get { return _city; }
            set { _city = value; }
        }

        public Vendor(string name, string city, string country)
        {
            Name = name;
            City = city;
            Country = country;
        }
        
    }
}