﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DeliveryMan.Models
{
    public class Order
    {
        public string Client { get; set; }

        public string Name { get; set; }

        public double Price { get; set; }

        public string Type { get; set; }

        public string Place { get; set; }

        public int Count { get; set; }

        public Order(string client, string name, double price, string type, string place, int count)
        {
            Client = client;
            Name = name;
            Price = price;
            Type = type;
            Place = place;
            Count = count;
        }
    }
}