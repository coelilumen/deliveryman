﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DeliveryMan.Models
{
    public class ItemsViewModel
    {
        public IList<Item> Items { get; set; }

        [Required(ErrorMessage="Поле является обязательным для заполнения.")]
        [StringLength(30, ErrorMessage = "Длина поля не может превышать 30 символов")]
        [Display(Name = "Название товара*")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Поле является обязательным для заполнения.")]
        [DataType("Double", ErrorMessage="Значением этого поля должно быть число.")]
        [Display(Name = "Цена товара*")]
        public double Price { get; set; }

        [Display(Name = "Поставщик")]
        public string Vendor { get; set; }

        public IList<Vendor> Vendors { get; set; }

        [Display(Name = "Категория")]
        public string Category { get; set; }

        public IList<String> Categories { get; set; }
    }
}