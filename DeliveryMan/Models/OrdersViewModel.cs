﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DeliveryMan.Models
{
    public class OrdersViewModel
    {
        public IList<Order> Orders { get; set; }

        public IList<string> Clients { get; set; }

        [Display(Name = "Заказчик*")]
        public string Client { get; set; }

        [Display(Name = "Товар*")]
        public string Item { get; set; }
        
        public IList<Item> Items { get; set; }

        [Display(Name = "Тип доставки*")]
        public string Type { get; set; }

        public IList<string> Types { get; set; }

        [Required(ErrorMessage = "Поле является обязательным для заполнения.")]
        [DataType("Int", ErrorMessage = "Значением этого поля должно быть целое число.")]
        [Display(Name = "Количество*")]
        public int Count { get; set; }
    }
}