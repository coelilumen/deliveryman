﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AnalysisServices.AdomdClient;
using System.Data;
using DeliveryMan.Models;

namespace DeliveryMan.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Orders()
        {
            DbHelper helper = new DbHelper();
            OrdersViewModel ovm = new OrdersViewModel();
            ovm.Orders = helper.GetAllOrders();
            ovm.Items = helper.GetAllItems();
            ovm.Clients = helper.GetAllClients();
            ovm.Types = helper.GetAllTypes();

            return View("Orders", ovm);
        }

        public ActionResult Items()
        {
            DbHelper helper = new DbHelper();
            ItemsViewModel ivm = new ItemsViewModel();
            ivm.Items = helper.GetAllItems();
            ivm.Vendors = helper.GetAllVendors();
            ivm.Categories = helper.GetAllCategories();

            return View("Items", ivm);
        }

        public ActionResult Vendors()
        {
            DbHelper helper = new DbHelper();
            return View("Vendors", helper.GetAllVendors());
        }


        [HttpPost]
        public ActionResult Form(ItemsViewModel model)
        {
            DbHelper helper = new DbHelper();
            ItemsViewModel ivm = new ItemsViewModel();
            ivm.Items = helper.GetAllItems();
            ivm.Items.Add(new Item(model.Name, model.Price, model.Vendor, model.Category, helper.GetAllVendors().FirstOrDefault(m => m.Name == model.Vendor).Country));
            ivm.Vendors = helper.GetAllVendors();
            ivm.Categories = helper.GetAllCategories();

            return View("Items", ivm);
        }

        public ActionResult Comments()
        {
            return View("Comments");
        }

    }
}